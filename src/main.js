import sudoku from "./sudoku.js";
import constants from "./constants.js";
import myDb from "./myDb.js";
import axios from "axios";
import VueAxios from "vue-axios";
import VueCordova from "vue-cordova";
import { createApp } from "vue";
import App from "./App.vue";
import firebaseApp from "firebase/app";
import firebaseStorage from "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyDlKjbHMOyjv1jyM3WilEydPOkJtD89tD0",
  authDomain: "musiclibrary-2c1d3.firebaseapp.com",
  projectId: "musiclibrary-2c1d3",
  storageBucket: "musiclibrary-2c1d3.appspot.com",
  messagingSenderId: "438224070666",
  appId: "1:438224070666:web:d4d82bdca8203979fb97f3",
  measurementId: "G-8BTLXW26JL",
};
// Initialize Firebase
var fbApp = firebaseApp.initializeApp(firebaseConfig);

// console.log("fbApp : ", fbApp);
console.log("firebaseStorage : ", firebaseStorage);

var app = createApp(App);
app.use(sudoku);
app.use(myDb);
app.use(constants);
app.use(VueCordova);
app.use(VueAxios, axios);
app.config.globalProperties = {
  firebaseStorage: fbApp.storage(),
  constants: app.constants,
  sudoku: app.sudoku,
  db: app.db,
  axios: app.axios,
  cordova: app.cordova,
  isDev: true, // In DEVELOPER Mode
  devValues: {
    ADMOB_BANNER_AD_PUBLISHER_ID: "ca-app-pub-3940256099942544/6300978111",
    ADMOB_INTERSTITIAL_AD_PUBLISHER_ID:
      "ca-app-pub-3940256099942544/8691691433",
    ADMOB_REWARD_AD_PUBLISHER_ID: "ca-app-pub-3940256099942544/5224354917",
  },
  prodValues: {
    ADMOB_BANNER_AD_PUBLISHER_ID: "ca-app-pub-5935995151626904/6574424707",
    ADMOB_INTERSTITIAL_AD_PUBLISHER_ID:
      "ca-app-pub-5935995151626904/3223951600",
    ADMOB_REWARD_AD_PUBLISHER_ID: "ca-app-pub-5935995151626904/3415523294",
  },
};
app.mount("#app");

console.log("app : ", app);
