const EASY_SCORE = 600;
const HARD_SCORE = 1200;
const MEDIUM_SCORE = 900;
const EXPERT_SCORE = 1800;
const APP_NAME = "SudokuPrakash";
const MY_MAIL = "rpapps25@gmail.com";
const PACKAGE_NAME = "com.whatsapp";
const MAIL_SUBJECT = APP_NAME + " feedback";
const MUSICFOLDER = "myMusic";
const HINT = "hint";
const TOKEN = "token";
const SECOND_CHANCE = "second-chance";
const NOTIFICATION_CHANNEL = {
  id: "NEW_MUSIC_ADDED",
  description: "New Music added.",
  name: "NEW MUSIC ADDED",
  sound: "notification_sound",
  vibration: true,
  light: true,
  lightColor: parseInt("FF0000FF", 16).toString(),
  importance: 4,
  badge: true,
  visibility: 1,
};

exports.install = function(Vue) {
  Vue.constants = {
    HINT,
    TOKEN,
    MY_MAIL,
    APP_NAME,
    EASY_SCORE,
    HARD_SCORE,
    MUSICFOLDER,
    EXPERT_SCORE,
    MAIL_SUBJECT,
    MEDIUM_SCORE,
    PACKAGE_NAME,
    SECOND_CHANCE,
    NOTIFICATION_CHANNEL,
  };
};
