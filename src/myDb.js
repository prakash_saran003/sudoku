var indexedDB =
  window.indexedDB ||
  window.mozIndexedDB ||
  window.webkitIndexedDB ||
  window.msIndexedDB;

var db_name = "rpapps_sudoku";
var versionNum = 1;
var request = indexedDB.open(db_name, versionNum);
var constIndex = "type, level";
var objStoreName = "sudoku_db";
var pointsAndTokenStoreName = "points_vs_tokens";
var clock_data = [
  {
    type: "classic",
    level: "easy",
  },
  {
    type: "classic",
    level: "medium",
  },
  {
    type: "classic",
    level: "hard",
  },
  {
    type: "classic",
    level: "expert",
  },
  {
    type: "lightening",
    level: "easy",
  },
  {
    type: "lightening",
    level: "medium",
  },
  {
    type: "lightening",
    level: "hard",
  },
  {
    type: "lightening",
    level: "expert",
  },
];
var pencil_data = [];
for (var i = 0; i < 9; i++) {
  var tmp2d = [];
  for (var j = 0; j < 9; j++) {
    var tmp3d = [];
    for (var k = 0; k < 9; k++) {
      tmp3d.push(0);
    }
    tmp2d.push(tmp3d);
  }
  pencil_data.push(tmp2d);
}

request.onupgradeneeded = function(e) {
  // console.log("onupgradeneeded  e:", e);
  var database = e.target.result;
  var version = parseInt(database.version);
  console.log("database : ", database, "  version:", version);
  var transaction = e.target.transaction;
  if (e.oldVersion < 1) {
    database.createObjectStore("theme", {
      keyPath: "key",
    });

    var sdk = database.createObjectStore(objStoreName, {
      autoIncrement: true,
    });
    sdk.createIndex(constIndex, ["type", "level"]);

    database.createObjectStore(pointsAndTokenStoreName, {
      autoIncrement: true,
    });

    database.createObjectStore("feedback", {
      keyPath: "key",
    });

    transaction.oncomplete = function() {
      addDataForVersion1(database);
    };
  }
  // if (e.oldVersion < 2) {
  //   database.createObjectStore(pointsAndTokenStoreName, {
  //     keyPath: "key",
  //   });
  //   transaction.oncomplete = function() {
  //     addDataForVersion2(database);
  //   };
  // }
};

request.onerror = function(event) {
  console.log("request.onerror object store creation ERROR: ", event);
};

var addDataForVersion1 = (db) => {
  var transaction = db.transaction("theme", "readwrite");
  var theme = transaction.objectStore("theme");

  var req = theme.add({
    key: "darkTheme",
    val: true,
  });
  req.onsuccess = function() {
    console.log("theme added to the store", request.result);
  };
  req.onerror = function() {
    console.log("Error", request.error);
  };

  var transaction1 = db.transaction(objStoreName, "readwrite");
  var main_db = transaction1.objectStore(objStoreName);
  clock_data.forEach((d, idx) => {
    var tmp = {
      id: idx + 1,
      type: d.type,
      level: d.level,
      clockAngle: 0,
      resumeTime: 0,
      ansMatrix: null,
      currMatrix: null,
      initialMatrix: null,
      hint: 2,
      hintUsed: 0,
      mistake: 0,
      pencilData: pencil_data,
      highScore: Infinity,
      currScore: Infinity,
      lighteningTimerState: null,
      secondChance: 0,
      helpObjUsed: {
        pencilAll: 0,
        showSpecificNum: 0,
        stopTimer: 0,
      },
    };
    if (d.type == "lightening") {
      tmp.highScore = 0;
    }
    var req1 = main_db.add(tmp);
    req1.onsuccess = function() {
      console.log("main_db added to the store", req1.result);
    };
    req1.onerror = function() {
      console.log("Error main_db not added to the store", req1.error);
    };
  });

  var transaction2 = db.transaction(pointsAndTokenStoreName, "readwrite");
  var tokenObj = transaction2.objectStore(pointsAndTokenStoreName);

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();

  today = dd + "/" + mm + "/" + yyyy;
  // console.log("MYDBBBBB : ", today);

  var req2 = tokenObj.add({
    tokens: 50,
    points: 0,
    today_date: today,
    tokenGifts: 3,
  });
  req2.onsuccess = function() {
    console.log("tokenObj added to the store", req2.result);
  };
  req2.onerror = function() {
    console.log("Error", req2.error);
  };

  var feedback_transaction = db.transaction("feedback", "readwrite");
  var feedback = feedback_transaction.objectStore("feedback");

  var feedback_req = feedback.add({
    key: "askAboutFeedback",
    val: true,
  });
  feedback_req.onsuccess = function() {
    console.log("feedback added to the store", request.result);
  };
  feedback_req.onerror = function() {
    console.log("Error", request.error);
  };
};
// var addDataForVersion2 = (db) => {
//   var transaction2 = db.transaction(pointsAndTokenStoreName, "readwrite");
//   var tokenObj = transaction2.objectStore(pointsAndTokenStoreName);

//   var req2 = tokenObj.add({
//     key: "tokens",
//     val: 50,
//   });
//   req2.onsuccess = function() {
//     console.log("tokenObj added to the store", req2.result);
//   };
//   req2.onerror = function() {
//     console.log("Error", req2.error);
//   };
// };

var updateTheme = (flag, callback) => {
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;
    var objectStore = db
      .transaction(["theme"], "readwrite")
      .objectStore("theme");
    var req = objectStore.get("darkTheme");
    req.onerror = function(event) {
      console.log("updateTheme req.onerror: ", event);
    };
    req.onsuccess = function(event) {
      // Get the old value that we want to update
      var data = event.target.result;
      // update the value(s) in the object that you want to change
      data.val = flag;

      // Put this updated object back into the database.
      var requestUpdate = objectStore.put(data);
      requestUpdate.onerror = function(event) {
        console.log("updateTheme requestUpdate.onerror: ", event);
      };
      requestUpdate.onsuccess = function() {
        // console.log("updateTheme requestUpdate.onsuccess: ", event);
        callback("success");
      };
      requestUpdate.onerror = function() {
        callback("fail");
      };
    };
  };
};

var getTheme = (callback) => {
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;

    var transaction = db.transaction(["theme"]);
    var objectStore = transaction.objectStore("theme");
    var request = objectStore.get("darkTheme");
    request.onerror = function(event) {
      console.log("getTheme request.onerror:", event);
    };
    request.onsuccess = function() {
      callback(request.result);
    };
  };
};

var getAskAboutFeedback = (callback) => {
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;

    var transaction = db.transaction(["feedback"]);
    var objectStore = transaction.objectStore("feedback");
    var request = objectStore.get("askAboutFeedback");
    request.onerror = function(event) {
      console.log("getTheme request.onerror:", event);
    };
    request.onsuccess = function() {
      callback(request.result);
    };
  };
};

var updateAskAboutFeedback = (flag, callback) => {
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;
    var objectStore = db
      .transaction(["feedback"], "readwrite")
      .objectStore("feedback");
    var req = objectStore.get("askAboutFeedback");
    req.onerror = function(event) {
      console.log("updateAskAboutFeedback req.onerror: ", event);
    };
    req.onsuccess = function(event) {
      // Get the old value that we want to update
      var data = event.target.result;
      // update the value(s) in the object that you want to change
      data.val = flag;

      // Put this updated object back into the database.
      var requestUpdate = objectStore.put(data);
      requestUpdate.onerror = function(event) {
        console.log("updateAskAboutFeedback requestUpdate.onerror: ", event);
      };
      requestUpdate.onsuccess = function() {
        // console.log("updateTheme requestUpdate.onsuccess: ", event);
        callback("success");
      };
      requestUpdate.onerror = function() {
        callback("fail");
      };
    };
  };
};

var updateTokenValue = (arg, callback) => {
  console.log("updateTokenValue arg:", arg);
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;

    var pts = db
      .transaction(pointsAndTokenStoreName, "readwrite")
      .objectStore(pointsAndTokenStoreName);
    var cursorRequest = pts.openCursor();
    cursorRequest.onsuccess = (e) => {
      var cursor = e.target.result;
      if (cursor) {
        // console.log("cursor : ", cursor);
        // console.log("cursor.value : ", cursor.value);
        var tmp = cursor.value;
        if (arg.tokens != undefined) {
          tmp.tokens = arg.tokens;
        }
        if (arg.points != undefined) {
          tmp.points += arg.points;
        }
        if (arg.today_date != undefined) {
          tmp.today_date = arg.today_date;
        }
        if (arg.tokenGifts != undefined) {
          tmp.tokenGifts = arg.tokenGifts;
        }
        // console.log("myDBBBB tmp : ", tmp);
        var updateRequest = cursor.update(tmp);
        updateRequest.onsuccess = () => {
          callback("success");
        };
        updateRequest.onerror = (err) => {
          console.log("e : ", err);
          callback("fail");
        };
        cursor.continue();
      }
    };
  };
};

var getTokenValue = (callback) => {
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;

    var transaction = db.transaction([pointsAndTokenStoreName]);
    var objectStore = transaction.objectStore(pointsAndTokenStoreName);

    var request = objectStore.getAll();

    request.onerror = function(event) {
      console.log("tokens request.onerror:", event);
    };
    request.onsuccess = function() {
      callback(request.result[0]);
    };
  };
};

var getSudokuStates = (callback) => {
  // console.log("getSudokuStates in myDb : ");
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;
    var sudoku = db.transaction(objStoreName).objectStore(objStoreName);
    // var priceIndex = sudoku.index(constIndex);
    // var req = priceIndex.get([arg.type, arg.level]);
    var req = sudoku.getAll();

    req.onsuccess = function() {
      if (req.result !== undefined) {
        callback(req.result);
      } else {
        console.log("No Data in " + objStoreName);
      }
    };
    req.onerror = function(err) {
      console.log("onerror in fetching data from " + objStoreName + " : ", err);
      callback("fail");
    };
  };
};

var saveSudokuStates = (arg, callback) => {
  // console.log("argggg : ", arg);
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;

    var sudoku = db
      .transaction(objStoreName, "readwrite")
      .objectStore(objStoreName);
    var cursorRequest = sudoku.openCursor();
    cursorRequest.onsuccess = (e) => {
      var cursor = e.target.result;
      if (cursor) {
        if (cursor.value.type == arg.type && cursor.value.level == arg.level) {
          if (arg.resumeTime != undefined && arg.clockAngle != undefined) {
            var tmp = cursor.value;
            if (arg.currScore != undefined) {
              tmp.currScore = arg.currScore;
            }
            if (arg.secondChance != undefined) {
              if (arg.secondChance == true) {
                tmp.secondChance++;
              } else {
                tmp.secondChance = 0;
              }
            }
            if (arg.lighteningTimerState != undefined) {
              tmp.lighteningTimerState = arg.lighteningTimerState;
            } else {
              tmp.lighteningTimerState = null;
            }
            if (arg.resumeTime != undefined) {
              tmp.resumeTime = arg.resumeTime;
            }
            if (arg.clockAngle != undefined) {
              tmp.clockAngle = arg.clockAngle;
            }
            if (arg.ansMatrix != undefined) {
              tmp.ansMatrix = arg.ansMatrix;
            }
            if (arg.currMatrix != undefined) {
              tmp.currMatrix = arg.currMatrix;
            }
            if (arg.initialMatrix != undefined) {
              tmp.initialMatrix = arg.initialMatrix;
            }
            if (arg.pencilData != undefined) {
              tmp.pencilData = arg.pencilData;
            }
            if (arg.mistake != undefined) {
              if (arg.mistake == 0) tmp.mistake = 0;
              else tmp.mistake += arg.mistake;
            }
            if (arg.hint != undefined) {
              if (arg.hint == 2) {
                tmp.hint = 2;
                tmp.hintUsed = 0;
              } else {
                tmp.hintUsed = tmp.hintUsed + 1;
                if (tmp.hint > 0) {
                  tmp.hint = tmp.hint - 1;
                } else {
                  tmp.hint = 0;
                }
              }
            }
            if (arg.helpObjUsed != undefined) {
              if (arg.helpObjUsed.showSpecificNum != undefined) {
                if (arg.helpObjUsed.showSpecificNum == 0) {
                  tmp.helpObjUsed.showSpecificNum = 0;
                } else {
                  tmp.helpObjUsed.showSpecificNum++;
                }
              }
              if (arg.helpObjUsed.pencilAll != undefined) {
                if (arg.helpObjUsed.pencilAll == 0) {
                  tmp.helpObjUsed.pencilAll = 0;
                } else {
                  tmp.helpObjUsed.pencilAll++;
                }
              }
              if (arg.helpObjUsed.stopTimer != undefined) {
                if (arg.helpObjUsed.stopTimer == 0) {
                  tmp.helpObjUsed.stopTimer = 0;
                } else {
                  tmp.helpObjUsed.stopTimer++;
                }
              }
            }
            // console.log("myDBBBB tmp : ", tmp);
            var updateRequest = cursor.update(tmp);
            updateRequest.onsuccess = () => {
              callback("success");
            };
            updateRequest.onerror = (err) => {
              console.log("e : ", err);
              callback("fail");
            };
          }
        }
        cursor.continue();
      }
    };
  };
};

var saveHighScore = (arg, callback) => {
  // console.log("argggg : ", arg);
  var request1 = indexedDB.open(db_name, versionNum);
  request1.onsuccess = (event) => {
    var db = event.target.result;

    var sudoku = db
      .transaction(objStoreName, "readwrite")
      .objectStore(objStoreName);
    var cursorRequest = sudoku.openCursor();
    cursorRequest.onsuccess = (e) => {
      var cursor = e.target.result;
      if (cursor) {
        if (cursor.value.type == arg.type && cursor.value.level == arg.level) {
          var tmp = cursor.value;
          if (arg.type == "classic" && tmp.highScore > arg.highScore) {
            tmp.highScore = arg.highScore;
          } else if (
            arg.type == "lightening" &&
            tmp.highScore < arg.highScore
          ) {
            tmp.highScore = arg.highScore;
          }
          // console.log("myDBBBB tmp : ", tmp);
          var updateRequest = cursor.update(tmp);
          updateRequest.onsuccess = () => {
            callback("success");
          };
          updateRequest.onerror = (err) => {
            console.log("e : ", err);
            callback("fail");
          };
        }
        cursor.continue();
      }
    };
  };
};

exports.install = function(Vue) {
  Vue.db = {
    getTheme,
    updateTheme,
    getTokenValue,
    saveHighScore,
    getSudokuStates,
    saveSudokuStates,
    updateTokenValue,
    getAskAboutFeedback,
    updateAskAboutFeedback,
  };
};

/*
 * Classic Sudoku
 * # clock,
 * # hint,
 * # mistake,
 * # matrix,
 * # ans matrix,
 * # initial_matrix,
 * # pencil,
 * # high score,
 *
 * Lightening Sudoku
 * #
 *
 */
